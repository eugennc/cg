#!/usr/bin/python3

import sys

import pygame
from pygame.locals import *

from OpenGL.GL import *

#needs a compiler to install, provided by PyOpenGL_accelerate
#from OpengGL.GLU import *

'''
if you don't have a C compiler:
pip install PyOpenGL pygame
if you do have a C compiler:
pip install PyOpenGL PyOpenGL_accelerate pygame

pyOpenGl reference at:
http://pyopengl.sourceforge.net/documentation/manual-3.0/index.html

pygame reference at e.g.:
https://www.pygame.org/docs/ref/event.html

'''

#lazy global variable
g_imageNumber = 1

def display1():
    glBegin(GL_LINES)
    glColor3f(0.9, 0.1, 0.1)
    glVertex2i( -1,  -1)
    glVertex2i(  1,   1)

    glColor3f(0.1, 0.1, 0.9)
    glVertex2i( -1,   1)
    glVertex2i(  1,  -1)

    glColor3f(0.9, 0.1, 0.9)
    glVertex2f( -0.5,  0)
    glVertex2f(  0.5,  0)
    glEnd()


def display2():
    glColor3f(0.9, 0.1, 0.1)
    glBegin(GL_LINES)
    glVertex2i( -1,  -1)
    glVertex2i(  0,   0)

    glVertex2f(0.3, 0.3)
    glVertex2f(0.5, 0.5)

    glVertex2d(0.6, 0.6)
    glVertex2d(0.8, 0.8)

    glVertex2d(0.95, 0.95)
    glVertex2d(  1,   1)
    glEnd()

def display3():
    glColor3f(0.9, 0.1, 0.1)
    glBegin(GL_POINTS)
    glVertex2f(0.9, 0.9)
    glVertex2f(0.6, 0.5)
    glVertex2f(0.3, 0.7)
    glEnd()
    
def display4():
    glColor3f(0.9, 0.1, 0.1)
    glBegin(GL_LINE_STRIP)
    glVertex2f(0.9, 0.9)
    glVertex2f(0.6, 0.5)
    glVertex2f(0.3, 0.7)
    glEnd()

def display5():
    glBegin(GL_LINE_LOOP)
    glEnd()


def display6():
    glColor3f(0.9, 0.1, 0.1)
    glBegin(GL_TRIANGLES)
    glVertex2f(0.9, 0.9)
    glVertex2f(0.6, 0.5)
    glVertex2f(0.3, 0.7)
    glEnd()

def display7():
    glColor3f(0.9, 0.1, 0.1)
    glBegin(GL_QUADS)
    glVertex2f(0.9, 0.9)
    glVertex2f(0.6, 0.5)
    glVertex2f(0.3, 0.7)
    glVertex2f(0.3, 0.71)
    glEnd()

def display8():
    glColor3f(0.9, 0.1, 0.1)
    glBegin(GL_POLYGON)
    glVertex2f( 0.9,  0.6)
    glVertex2f( 0.6,  0.5)
    glVertex2f( 0.3,  0.7)
    glVertex2f( 0.7,    1)
    glVertex2f(0.75, 0.95)
    glEnd()



def display(imageNumber):
    #clears the image
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    if   imageNumber == 1:
        display1()
    elif imageNumber == 2:
        display2()
    elif imageNumber == 3:
        display3()
    elif imageNumber == 4:
        display4()
    elif imageNumber == 5:
        display5()
    elif imageNumber == 6:
        display6()
    elif imageNumber == 7:
        display7()
    elif imageNumber == 8:
        display8()

    #make sure the image is displayed
    glFlush()

        

def keyDown(key):
    print('Keypress:', key)
    if key == 27:
        pygame.quit()
        quit()
    elif key >= ord('1') and key <= ord('8'):
        global g_imageNumber
        g_imageNumber = key - ord('0')
        print('Will now render image', g_imageNumber)


def mouseDown(pos, button):
    x, y = pos
    print('Mouse button %d press at (%d, %d)' % (button, x, y));

def resize(size):
    w, h = size
    print('Window resized to', size)
    glViewport(0, 0, GLsizei(w), GLsizei(h))

def mainLoop():
    '''A pygame event loop which consumes and processes events from the pygame event queue'''
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
                
            elif event.type == pygame.KEYDOWN:
                keyDown(event.key)
                
            elif event.type == pygame.KEYUP:
                pass
            
            elif event.type == pygame.MOUSEBUTTONDOWN:
                mouseDown(event.pos, event.button)

            elif event.type == pygame.VIDEORESIZE:
                resize(event.size)
                
        display(g_imageNumber)
        
        pygame.time.wait(10)


def init():
    #set the color used to erase the display
    glClearColor(1.0,1.0,1.0,1.0)

    #set the width of lines being drawn
    glLineWidth(3)

    #set the thickness of points being drawn
    glPointSize(4)

    #sets the polygon draw / fill mode
    #the first parameter specifies the side being modified: GL_FRONT, GL_BACK or GL_FRONT_AND_BACK
    #it is determined in the proper mathematical way, by using the right-hand rule and depends
    #on vertex order
    #
    #the second parameter specifies the draw mode for that side:
    #GL_POINT for points in the polygon vertices
    #GL_LINE for drawing the edges of the polygon
    #GL_FILL for filling the inside of the polygon
    glPolygonMode(GL_FRONT, GL_LINE);

#-------------------------
def main():
    #init the library
    pygame.init()
    
    #a tuple containing the display size
    display = (800, 600)
    
    #init the display size and properties; we specifically request an OpenGL context and a resizable window
    pygame.display.set_mode(display, pygame.RESIZABLE | pygame.OPENGL) # pygame.DOUBLEBUF, pygame.NOFRAME ...
    
    #set the window name
    pygame.display.set_caption('Python OpenGL Example')

    #our custom init
    init()

    #launch the event loop
    mainLoop()


main()
pygame.quit()
quit()
